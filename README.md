# BibleCoverage

Keep track of your Bible reading over time, and get a visual overview of the parts of the Bible that you've not explored recently.
- Links to bible.com or the YouVersion app for reading.
- Choose from a number of color themes.